/* Copyright (C) 2021-2022 Jack Dodds
 * This code is part of CryptLayer and is hereby released under the
 * Gnu General Public Licence v.3.
 * See GPLv3.txt and https://www.gnu.org/licenses/gpl-3.0.txt
 *
 * Outline:
 *  public final class CLImapProxy implements Runnable
 *      public static class ImapByteArray
 *      public static interface ImapFilter
 *  final class CLImapConnection
 *      private class ImapClntToSrvr implements Runnable
 *      private class ImapSrvrToClnt implements Runnable
 *      private class ImapByteArrayReader extends BufferedInputStream
 *      private class ImapByteArrayWriter extends BufferedOutputStream
 */

package net.brmdamon.cryptlayer;

import javax.net.ssl.SSLSocketFactory;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.util.concurrent.Semaphore;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import net.brmdamon.cryptlayer.CryptLayer.CLLogID;      // To eliminate need to fully qualify names.
import net.brmdamon.cryptlayer.CryptLayer.CLLog;        // To eliminate need to fully qualify names.

/**
 * CLImapProxy sets up an IMAP proxy that listens for a connection from an IMAP
 * client on a specified port, opens a socket from the client and a TLS socket
 * to a specified IMAP server. A thread is created for the connection, which in
 * turn creates an IMAP client to server (ICS) thread to pass the stream from
 * the client to the server socket, and a companion IMAP server to client (ISC)
 * thread to pass the stream from the server to the client socket. An ImapFilter
 * object can be provided to filter the two streams to implement cryptographic
 * or other functions. Each connection is provided with a distinct ImapFilter
 * instance. Once the two threads are set up, this class continues to listen for
 * new client connections, and set up socket pairs accordingly, so multiple
 * simultaneous connections are possible. When the client or the server
 * disconnects, the associated sockets are closed and the associated threads
 * terminate.
 * 
 * In comments, some capitalized terms are used to more concisely refer to 
 * terms from RFC9051:
 * Command client command
 * Continuation - command continuation
 * Response - server response
 * Request - command continuation request NOT for a synchronizing literal.
 * Literal Request - command continuation request related to a literal.
 */
public final class CLImapProxy implements Runnable {

    /**
     * CLImapProxy.ImapByteArray is used to efficiently receive, process and
     * write IMAP byte streams using LinkedLists of byte arrays of a specified
     * maximum length BYTE_ARRAY_MAX. A Command, Response, Request or Literal
     * begins at the beginning of a byte array and occupies one or more
     * consecutive byte arrays in the LinkedList. Commands, Responses, Requests
     * or Literals each end with a byte array of length less than BYTE_ARRAY_MAX 
     * (possibly zero), otherwise byte arrays are of length BYTE_ARRAY_MAX.
     * If a tag occurs at the beginning of an array the tag member must contain
     * it. Members octetCount and isSynchronizing must be set in the last array
     * of the LinkedList containing them.
     */
    public static class ImapByteArray {

        public static final int BYTE_ARRAY_MAX = 2048;  // Maximum size of body.
        
        // Attributes must always be mutually consistent.
        long octetCount = -1;                           // Value of literal octet count at end of body, or -1 if none.
        boolean isSynchronizing = false;                // Octet count contains '+' flag.
        byte[] tag = new byte[0];                       // Tag at beginning of body, or byte[0].
        File longLiteralFile = null;                    // Unimplemented: body is in temporary file, not byte array.
        public byte[] body;                             // Segment of command or response.

        public ImapByteArray(int arraySize) {
            body = new byte[arraySize];
        }
    }

    /**
     * CLImapProxy.ImapFilter defines the interface to a user supplied filter
     * that sits between the client and server, modifying Commands and
     * Responses. Each connection is assigned a unique new ImapFilter instance
     * which CryptLayer obtains via the user supplied method makeNewInstance().
     * CryptLayer calls the user supplied methods toServer() and fromServer()
     * to respectively filter commands to the server and responses from the
     * server. It is permitted for the methods to modify the Command or
     * Response to be an empty list. Methods toServer() and fromServer()
     * also accept a parameter which can be set to a Response or Command which
     * will be injected into the opposite direction stream, to the client or
     * server respectively. If the method does not set that parameter to a
     * non-empty list, nothing is injected.
     */
    public interface ImapFilter {
        /** @return Returns a unique new instance of the filter.
         * CLImapProxy can't do this with "new" because the user's filter
         * class constructor is not visible to it. Could be done with Clone
         * or java.lang.reflect.Constructor.newInstance() but this is simpler.
         */
        ImapFilter makeNewInstance();

        /** Filters Commands to server and injects Responses to client. */
        void toServer(CLLogID id, LinkedList<ImapByteArray> command, LinkedList<ImapByteArray> responseToInject);

        /** Filters responses from server and injects commands to server. */
        void fromServer(CLLogID id, LinkedList<ImapByteArray> response, LinkedList<ImapByteArray> commandToInject);
    }

    private InetAddress proxyHost;
    private int proxyPort;
    private InetAddress srvrHost;
    private int srvrPort;
    private Semaphore connCount; // For threadsafe counting of connections.

    private ImapFilter userFilter0; // A filter instance used only to create new instances (may be null).

    /* Variables maintained to support logging of the proxy state and operations. */
    private CLLog logger;
    private CLLogID idIP;
    private static int instance = 0; // Serial number of thread pairs created.

    /**
     * @param id          Identification of message source for logger.
     * @param proxyHost   Address and port where CLImapProxy listens for client.
     * @param proxyPort
     * @param srvrHost    Address and port of IMAP server.
     * @param srvrPort
     * @param connMax     Maximum number of simultaneous connections permitted.
     * @param userFilter0 Instance of the user's filter, used to create a new
     *                    instance for each connection. Null if none.
     * @param logger      Logger.
     */
    public CLImapProxy(CLLogID id, InetAddress proxyHost, int proxyPort, InetAddress srvrHost,
            int srvrPort, int connMax, ImapFilter userFilter0, CLLog logger) {

        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.srvrHost = srvrHost;
        this.srvrPort = srvrPort;
        this.connCount = new Semaphore(connMax);
        this.userFilter0 = userFilter0;

        this.logger = logger;
        idIP = new CLLogID(id);
        idIP.type = CLLogID.IMAP;
        idIP.thread = CLLogID.CONTROL;
    }

    /**
     * The main thread of the IMAP proxy.
     */
    public void run() {
        SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        ServerSocket listenSock = null;

        int logEvent = CLLogID.EXCP;

        this.logger.log(idIP, CLLogID.INFO, "IMAP Listener Info");
        CryptLayer.infoLstnAddr(idIP, this.proxyHost, this.logger);
        try {
            /* Create an unencrypted socket to receive client connection. */
            logEvent = CLLogID.EXOPENLSTN;
            listenSock = new ServerSocket(this.proxyPort, 1, this.proxyHost); // Possible IOException.
            do {
                Socket clntSock = null;
                SSLSocket srvrSock = null;

                instance++;
                idIP.instance = instance;
                this.connCount.acquireUninterruptibly();

                /* Connect a socket to the client when requested. */
                logEvent = CLLogID.EXOPENCLNT;
                clntSock = listenSock.accept();         // Possible IOException.
                this.logger.log(idIP, CLLogID.ACCEPT, "Accepted IMAP client connection ");

                /* Connect a TLS socket to the IMAP server. */
                logEvent = CLLogID.EXOPENSRVR;
                srvrSock = (SSLSocket) sslsocketfactory.createSocket(this.srvrHost, this.srvrPort); // IOException?
                this.logger.log(idIP, CLLogID.INFO, "IMAP TLS Server Info");
                CryptLayer.infoSocketToSrvr(idIP, srvrSock, this.logger);
                this.logger.log(idIP, CLLogID.CONNECT, "Made IMAP server connection");

                /*
                 * Start subthreads for streams to and from the server. They terminate when
                 * either clntSock or srvrSock is closed.
                 */

                new CLImapConnection(idIP, clntSock, srvrSock, this.userFilter0, this.connCount, this.logger);
            } while (true);
        } catch (IOException err) {
            this.logger.log(idIP, logEvent, err);
        }
        try {
            listenSock.close();
        } catch (IOException err) {
            this.logger.log(idIP, CLLogID.EXCP, err);
        }
    }
}

/**
 * CLImapConnection manages one connection between an IMAP client and an IMAP
 * server. It accepts open sockets, one from an IMAP client and the other to an
 * IMAP server. It sets up two new threads, the IMAP client to server (ICS)
 * thread to pass the stream from the client socket to the server socket and a
 * companion IMAP server to client thread (ISC) thread to pass the stream from
 * the server socket to the client socket.
 * It provides facilities for the stream data to be filtered on the way through
 * to implement cryptographic operations. Also it allows the ISC thread to
 * inject Commands into the ICS stream and then to intercept the server
 * Response and similarly to inject its own Responses to client Commands.
 * If an exception occurs in either thread, specifically when either the client
 * or the server disconnects, that thread closes its client and server sockets
 * and terminates. Since the sockets are closed, the companion thread will have
 * an exception and terminate.
** 
 */
final class CLImapConnection {

    // This saves writing the fully qualified name over and over.
    static final int BYTE_ARRAY_MAX = CLImapProxy.ImapByteArray.BYTE_ARRAY_MAX;
    
    static final long MAX_OCTET_COUNT = 1000000000000L; // Maximum literal octet count < 2^63/10 -10
    static final int MAX_TAG_BYTES = 15;                // Maximum size of a command tag.
    static final byte[] IDLE_COMMAND = "IDLE".getBytes();   // *** Is it always upper case?
    static final byte[] LITERAL_REQUEST = "+ Ready for literal data - injected".getBytes();
    static final byte[] REQUEST_TAG = "+".getBytes();
    
    private BufferedInputStream clntRdr;
    private BufferedOutputStream srvrWrtr;
    private BufferedInputStream srvrRdr;
    private BufferedOutputStream clntWrtr;
    private CLImapProxy.ImapFilter userFilter;
    private Thread clntToSrvrThread = null;
    private Thread srvrToClntThread = null;
    
    /* The following are used by sendCommand() and getResponse() when sending
     * synchronizing literals from client to server so that the ImapFilter
     * class does not have to implement the use of Literal Requests.  See
     * RFC9051-2.2.1. The handling of commands like AUTHETICATE is not affected.
     */
    
    private byte[] tagRequest = new byte[0];            // Tag of command that needs Literal Request.
    private boolean receivedRequest = false;            // True if Literal Request received.
    private Semaphore waitRequest = new Semaphore(1);   // Released to end wait for Literal Request.
    
    /* sendResponse() sets requestSent true before sending a Request to the
     * client. getCommand() uses the true value to identify a Continuation
     * then sets it false.
     * If the server has sent a Continuation, it must have received a Command
     * from the client that requires a Continuation, so the message from the
     * client must be that Continuation. See RFC9051-2.2.1,5.5.
     */
    boolean requestSent = false;
    
    private Semaphore connCount;                        // This is decremented (released) when connection closes.
    private CLLogID idIC;
    private CLLog logger;

    /* If not empty, then at least one command has not received a tagged Response.*/
    List<byte[]> activeTags = Collections.synchronizedList(new LinkedList<byte[]>());

    public CLImapConnection(CLLogID id, Socket clntSock, Socket srvrSock, CLImapProxy.ImapFilter userFilter0,
                                Semaphore connCount, CLLog logger) {
        this.connCount = connCount;
        this.idIC = new CLLogID(id);
        this.logger = logger;

        /* Make a new instance of the filter. */
        this.userFilter = null;
        if (userFilter0 != null) this.userFilter = userFilter0.makeNewInstance();

        try {
            this.clntRdr = new BufferedInputStream(clntSock.getInputStream());      // Possible IOException.
            this.srvrWrtr = new BufferedOutputStream(srvrSock.getOutputStream());   // Possible IOException.
            this.srvrRdr = new BufferedInputStream(srvrSock.getInputStream());      // Possible IOException.
            this.clntWrtr = new BufferedOutputStream(clntSock.getOutputStream());   // Possible IOException.
        } catch (IOException err) {
            this.logger.log(this.idIC, CLLogID.EXCP, err);
        }
        this.clntToSrvrThread = new Thread(new ImapClntToSrvr());
        this.srvrToClntThread = new Thread(new ImapSrvrToClnt());
        this.clntToSrvrThread.start();
        this.srvrToClntThread.start();
        this.logger.log(this.idIC, CLLogID.CONN, "IMAP connection started");
    }

    /**
    * getLine() gets a line ending with "\r\n" from the server or client
    * and appends it to an IMAP byte array list.
    * If a tag is expected, it identifies it at the start of the line and
    * returns it in the tag member of the array that contains it.
    * It parses any octet count at the end of the line and returns it and
    * the isSynchronizing member of the last element of the list.
    */
   private void getLine(BufferedInputStream reader, LinkedList<CLImapProxy.ImapByteArray> message, boolean getTag) 
           throws IOException {
       int lineLength;
       byte oneByte;
       byte previousByte = 0;
       boolean isDigit = false;
       boolean previousIsDigit = false;
       boolean isSynchronizing = false;
       long octetCount = -1;
       int tagLength = 0;
       CLImapProxy.ImapByteArray oneArray;
       CLImapProxy.ImapByteArray firstArray = null;
       
       if (getTag) tagLength = -1;

       // Read line, or end of a line after literal, into array(s)
       // BYTE_ARRAY_MAX long except last is <BYTE_ARRAY_MAX long.
       do {
           reader.mark(BYTE_ARRAY_MAX);
           lineLength = 0;
       
           // Find EOL or BYTE_ARRAY_MAX bytes whichever comes first.
           do {
               oneByte = (byte) reader.read();
               if (oneByte < 0) throw new IOException();
               lineLength++;
               
               if (lineLength <= MAX_TAG_BYTES && tagLength == -1 && oneByte == ' ') tagLength = lineLength - 1;

               // Parse and save octet count.
               if (oneByte == '{') {
                   octetCount = 0;
                   previousByte = 0;
                   previousIsDigit = false;
                   isSynchronizing = false;
               } else if (octetCount >= 0) {
                   isDigit = oneByte >= '0' && oneByte <= '9';
                   if (isDigit && (previousIsDigit || previousByte == '{')) {
                       octetCount = 10 * octetCount + oneByte - '0';
                       if (octetCount > MAX_OCTET_COUNT) octetCount = -1;
                   } else if (oneByte == '+' && previousIsDigit ||
                           oneByte == '}' && (previousIsDigit || previousByte == '+') ||
                           oneByte == '\r' && previousByte == '}' ||
                           oneByte == '\n' && previousByte == '\r') {
                       isSynchronizing |= oneByte == '+';
                   } else {
                       octetCount = -1;
                   }
                   previousByte = oneByte;
                   previousIsDigit = isDigit;
               }                        
           } while (lineLength < BYTE_ARRAY_MAX && oneByte != '\n');

           // Read to EOL or BYTE_ARRAY_MAX.
           reader.reset();                       // Go back to start of line. Possible IOException
           oneArray =  new CLImapProxy.ImapByteArray(lineLength);
           if (reader.read(oneArray.body) != lineLength) throw new IOException();
           message.add(oneArray);
           if (firstArray == null) firstArray = oneArray;
       } while (oneByte != '\n');
       // If the line length is an exact multiple of BYTE_ARRAY_MAX, mark the end with a zero length array.
       if (lineLength == BYTE_ARRAY_MAX) {
           oneArray =  new CLImapProxy.ImapByteArray(0);
           message.add(oneArray);
       }

       // Save tag and octet count.
       if( tagLength > 0) firstArray.tag = Arrays.copyOfRange(firstArray.body, 0, tagLength);
       oneArray.octetCount = octetCount;
       oneArray.isSynchronizing = isSynchronizing;
   }
   
   /**
   * getLiteral() gets a number of bytes specified by an octet count
   * from the server or client and appends it to an IMAP message.
   */
    private void getLiteral(BufferedInputStream reader, LinkedList<CLImapProxy.ImapByteArray> message,
            long octetCount) throws IOException {
        CLImapProxy.ImapByteArray byteArrayNext;
        int bytesToRead;
        int bytesRead;
        int bytesIn;
        
        while (octetCount > 0) {
            bytesToRead = (int) Math.min(octetCount, BYTE_ARRAY_MAX);
            bytesRead = 0;
            byteArrayNext = new CLImapProxy.ImapByteArray(bytesToRead);
            while (bytesRead < bytesToRead) {   // Multiple reads may be required to fill array.
                // Blocks until at least one byte available. Possible IOException.
                bytesIn = reader.read(byteArrayNext.body, bytesRead, bytesToRead - bytesRead);
                if (bytesIn <= 0) throw new IOException();
                bytesRead += bytesIn;
            }
            message.add(byteArrayNext);
            octetCount -= bytesToRead;
            // If octetCount was exact multiple of BYTE_ARRAY_MAX, mark end of literal with 0 length array.
            if (octetCount == 0 && bytesToRead == BYTE_ARRAY_MAX)
                message.add(new CLImapProxy.ImapByteArray(0));
        }
        return;
    }

    /**
     * getCommand() gets a Command or Completion from the client.
     * When a line ending with a synchronizing literal octet count is received,
     * it injects a Literal Request to the client so the client will send the
     * literal.
     */
    private void getCommand(LinkedList<CLImapProxy.ImapByteArray> command) 
        throws IOException {

        long octetCount;

        // Get first line.
        command.clear();
        getLine(this.clntRdr, command, !this.requestSent);
        this.requestSent = false;

        // Get literal(s)
        octetCount = command.getLast().octetCount;
        while (octetCount >= 0) {
            // To get a synchronizing literal, inject Literal Request to the client.
            if (command.getLast().isSynchronizing) {
                synchronized (this.clntWrtr) {
                    this.clntWrtr.write(LITERAL_REQUEST);
                }
                this.clntWrtr.flush();
            }
            getLiteral(this.clntRdr, command, octetCount);
            getLine(this.clntRdr, command, false);
            octetCount = command.getLast().octetCount;
        }
    };

    /**
     * sendResponse() sends a complete Response or Request to the client.
     * Literal Requests from the server should not be sent to the client
     * because getCommand() injects them when needed, so the response
     * parameter must not contain Literal Requests.
     */
    private void sendResponse(LinkedList<CLImapProxy.ImapByteArray> response) 
            throws IOException {
        if (response.size() == 0) return;
        synchronized(this.clntWrtr) {
            while (response.size() > 0) {
                // If a Request has been sent, signal getCommand().
                this.requestSent |= Arrays.equals(response.getFirst().tag, REQUEST_TAG);
                this.clntWrtr.write(response.removeFirst().body);   // Possible IOException.
            }
        }
        this.clntWrtr.flush();                               // *** Synchronized??
   }

    /**
     * getResponse() gets a complete Request or Response, including untagged
     * Responses and literals, up to and including the first Request or tagged
     * Response() received -except- IF there are no active tags (no commands
     * other than IDLE that have been sent to the server and have not received
     * a tagged Response) THEN it returns after getting an untagged response.
     * When it receives a Literal Request, it discards it, but signals
     * sendCommand() and continues reading.
     * When it receives a tagged Response it removes the tag from the list of
     * active tags.
     */
    
    private void getResponse(LinkedList<CLImapProxy.ImapByteArray> response) 
            throws IOException {

        byte[] tag;
        boolean isRequest;
        boolean isUntagged;
        long octetCount;
        int tagPosition;

        response.clear();
        do {
            // Get a single Response or Request.
            do {
                // Get first line of this Response or Request.
                tagPosition = response.size();
                getLine(this.srvrRdr, response, true);
                
                tag = response.get(tagPosition).tag;
                isRequest = Arrays.equals(tag, new byte[] {'+'});
                isUntagged = Arrays.equals(tag, new byte[] {'*'}); 
                
                // Signal sendCommand() if this is an expected Literal Request or tagged BAD Response. 
                if (this.tagRequest.length > 0 && isRequest || Arrays.equals(tag, this.tagRequest) ) {
                    // Signal sendCommand()
                    this.receivedRequest = isRequest;
                    this.waitRequest.release();
                    // Discard if Literal Request.
                    if (isRequest) {
                        response.removeLast();
                        isRequest = false;
                        break;
                    }
                }

                // Get literal(s)
                octetCount = response.getLast().octetCount;
                while (octetCount >= 0) {
                    getLiteral(this.srvrRdr, response, octetCount);
                    getLine(this.srvrRdr, response, false);
                    octetCount = response.getLast().octetCount;
                }
            } while (octetCount > 0);
        } while (response.size() == 0 || this.activeTags.size() != 0 && isUntagged);
        
        // Remove tag from list of active tags.
        if (!isUntagged) {
            for (int i = 0; i < this.activeTags.size(); i++ ) {
                if (Arrays.equals(tag, this.activeTags.get(i))) {
                    this.activeTags.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * sendCommand() sends a complete Command or Completion including
     * literals to the server. It adds any command tag to activeTags.
     * After sending the octet count for a synchronizing literal it waits for
     * a signal from getResponse to indicate that either a Literal Request,
     * or a tagged Response matching the tag of the Command being sent, has
     * been received. In the latter case the Response must be BAD (since no
     * Literal Request was received) so the remainder of the Command is
     * discarded.
     * *** What if a Continuation contains a synchronizing literal?
     */
    private void sendCommand(LinkedList<CLImapProxy.ImapByteArray> command) 
            throws IOException {
        byte[] tag;
        boolean isSynchronizing;
        
        if (command.size() == 0) return;
        tag = command.getFirst().tag;

        // Add tag to list of active tags, but not if this is an IDLE command.
        if (tag.length > 0 && !Arrays.equals(IDLE_COMMAND,
                    Arrays.copyOfRange(command.getFirst().body, tag.length + 1, tag.length + 5))) {
            this.activeTags.add(tag);
        }
        
        synchronized(this.srvrWrtr) {
            while (command.size() > 0) {
                isSynchronizing = command.getFirst().isSynchronizing;
                if (isSynchronizing) {
                    this.tagRequest = tag;              // Tag of Command waiting Literal Request..
                    this.waitRequest.tryAcquire();      // getResponse() releases on Literal Request or matching tag.
                }

                this.srvrWrtr.write(command.removeFirst().body);  // Possible IOException.

                if (isSynchronizing) {                  // Implement wait for Literal Request.
                    this.srvrWrtr.flush();              // Possible IOException.
                    this.waitRequest.acquireUninterruptibly();  // Wait for Literal Request or matching tag.
                    this.tagRequest = new byte[0];
                    // If the wait ends with a tagged (BAD) Command, remainder of Command is discarded.
                    if (!this.receivedRequest) break;
                }
            }
            command.clear();
        }
        this.srvrWrtr.flush();
    }

    /**
     * ImapClntToSrvr provides the Runnable for a thread that transfers
     * commands from the client to the server, filtering where required.
     */
    private class ImapClntToSrvr implements Runnable {
        private CLLogID idCS;

        ImapClntToSrvr() {
            this.idCS = new CLLogID(idIC);
            this.idCS.thread = CLLogID.TOSRVR;
            return;
        }

        public void run() {

            LinkedList<CLImapProxy.ImapByteArray> command = new LinkedList<CLImapProxy.ImapByteArray>();
            LinkedList<CLImapProxy.ImapByteArray> responseToInject = new LinkedList<CLImapProxy.ImapByteArray>();

            logger.log(this.idCS, CLLogID.START, "ICS started");
            try {
                /*
                 * Receive commands from client and pass to server until
                 * client or server socket is closed.
                 */
                while (true) {
                    getCommand(command);                    // Receive command from client.
                    if (command.size() == 0) break;
                    logger.log(this.idCS, CLLogID.BODY, command);

                    if (userFilter != null) {
                        responseToInject.clear();           // Implement filtering.
                        
                        userFilter.toServer(new CLLogID(this.idCS), command, responseToInject);
                        
                        if (responseToInject.size() > 0) sendResponse(responseToInject);
                    }

                    sendCommand(command);
                }
            } catch (IOException err) {
                logger.log(this.idCS, CLLogID.RUN, err);
            }
            /*
             * Zero length command or exception indicates client or server has disconnected.
             * Terminate thread after closing both sockets to trigger termination of the ISC
             * thread.
             */
            try {
                clntRdr.close();
            } catch (IOException err) {
            }
            try {
                srvrWrtr.close();
            } catch (IOException err) {
            }
            logger.log(this.idCS, CLLogID.STOP, "ICS stopped");
        }
    }

    /**
     * ImapSrvrToClnt provides the Runnable for a thread that transfers Responses
     * from the server to the client, filtering where required.
     */
    private class ImapSrvrToClnt implements Runnable {
        CLLogID idSC;

        ImapSrvrToClnt() {
            this.idSC = new CLLogID(idIC);
            this.idSC.thread = CLLogID.TOCLNT;
            return;
        }

        public void run() {

            LinkedList<CLImapProxy.ImapByteArray> responses = new LinkedList<CLImapProxy.ImapByteArray>();
            LinkedList<CLImapProxy.ImapByteArray> commandToInject = new LinkedList<CLImapProxy.ImapByteArray>();

            logger.log(this.idSC, CLLogID.START, "ISC started");
            try {
                /*
                 * Receive Responses from server and pass to client until
                 * client or server socket is closed.
                 */
                while (true) {
                    getResponse(responses);         // Possible IOException.
                    logger.log(this.idSC, CLLogID.BODY, responses);
                    
                    if (userFilter != null) {           // Implement filtering.
                        commandToInject.clear();
                        userFilter.fromServer(new CLLogID(this.idSC), responses, commandToInject);
                        if (commandToInject.size() > 0) sendCommand(commandToInject);
                    }

                    sendResponse(responses);
                }
            } catch (IOException err) {
                logger.log(this.idSC, CLLogID.RUN, err);
            }
            /*
             * Null or zero length or exception indicates client or server has disconnected.
             * Terminate thread after closing both sockets to trigger termination of the ISC
             * thread and waiting for the ISC tread to terminate.
             */
            try {
                clntRdr.close();
            } catch (IOException err) {
            }
            try {
                srvrRdr.close();
            } catch (IOException err) {
            }
            try {
                clntToSrvrThread.join();
            } catch (InterruptedException err) {
                logger.log(this.idSC, CLLogID.EXCP, err);
            }
            connCount.release();

            logger.log(this.idSC, CLLogID.STOP, "ICS and ISC stopped");
       }
    }
}
