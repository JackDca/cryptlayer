/* Copyright (C) 2021-2022 Jack Dodds
 * This code is part of CryptLayer and is hereby released under the
 * Gnu General Public Licence v.3.
 * See GPLv3.txt and https://www.gnu.org/licenses/gpl-3.0.txt
 *
 * Outline:
 *  public final class CLSmtpProxy implements Runnable
 *  public static class SmtpListOfByteArray extends LinkedList<byte[]>
 *  final class CLSmtpConnection
 *      private class SmtpClntToSrvr implements Runnable
 *      private class SmtpSrvrToClnt implements Runnable
 *      private class SmtpByteArrayReader extends BufferedInputStream
 */

package net.brmdamon.cryptlayer;

import javax.net.ssl.SSLSocketFactory;

import javax.net.ssl.SSLSocket;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import net.brmdamon.cryptlayer.CryptLayer.CLLogID;      // To eliminate need to fully qualify names.


/**
 * This class sets up an SMTP proxy that listens for connections from SMTP
 * clients on a specified port. When a client connects, this class in turn
 * connects to the specified SMTP server. It sets up two threads, the SMTP
 * client to server (SCS) thread to pass the stream from the client socket to
 * the server server socket and a companion SMTP server to client thread (SSC)
 * thread to pass the stream from the server socket to the client socket. The
 * stream data can be processed in the two threads to implement cryptographic
 * operations. Once the two threads are set up, this class continues to listen
 * for client connections, and set up pairs of threads accordingly, so multiple
 * simultaneous connections are possible. If an exception occurs in any thread,
 * specifically when either the client or the server disconnects, that thread
 * closes its client and server sockets and terminates. Since the sockets are
 * closed, the companion thread will have an exception and terminate.
 */
public final class CLSmtpProxy implements Runnable {

    /**
     * CLSmtpProxy.SmtpListOfByteArray is used to efficiently receive, process and
     * write SMTP byte streams using byte arrays of a specified maximum length
     * BYTE_ARRAY_MAX. Commands and reply lines each end with a byte array of length
     * less than BYTE_ARRAY_MAX (possibly zero), otherwise byte arrays are of length
     * BYTE_ARRAY_MAX.
     */
    public static class SmtpListOfByteArray extends LinkedList<byte[]> {

        public static final int BYTE_ARRAY_MAX = 1024;

        SmtpListOfByteArray() {
            super(new LinkedList<byte[]>());
        }

        /*
         * Compare the beginning of a list of byte arrays with a code.
         * The codelength must be < BYTE_ARRAY_MAX.
         */
        public boolean startsWith(byte[] code) {
            byte[] first;

            if (this.size() == 0)
                return false;
            first = this.getFirst();

            if (first.length < code.length)
                return false;
            for (int index = 0; index < code.length; index++)
                if (first[index] != code[index])
                    return false;
            return true;
        }
    }

    /**
     * CLSmtpProxy.SmtpFilter defines the interface to a user supplied filter that
     * sits between the client and server, modifying commands and replies. Each
     * connection is assigned a unique new SmtpFilter instance which CryptLayer
     * obtains via the user supplied method makeNewInstance(). CryptLayer calls the
     * user supplied methods toServer() and fromServer() to respectively filter
     * commands to the server and replies from the server. It is permitted for the
     * methods to modify the command or reply to be an empty list. Methods
     * toServer() and fromServer() also accept a parameter which can be set to a
     * reply or command which will be injected into the opposite direction stream,
     * to the client or server respectively. If the method does not set that
     * parameter to a non-empty list, nothing is injected.
     */
    public static interface SmtpFilter {
        /** Returns a unique new instance of the filter.
         * This could be done with java.lang.reflect.Constructor.newInstance()
         * or Clone but this seems a lot simpler.
         */
        SmtpFilter makeNewInstance();

        /** Filters commands to the server and injects replies to client. */
        void toSmtpServer(SmtpListOfByteArray command, SmtpListOfByteArray responseToInject);

        /** Filters replies from the server and injects commands to the server. */
        void fromSmtpServer(SmtpListOfByteArray reply, SmtpListOfByteArray commandToInject);
    }

    InetAddress proxyHost;
    int proxyPort;
    InetAddress srvrHost;
    int srvrPort;
    int connMax;
    Semaphore connCount;
    int connWait;
    int instance;

    private SmtpFilter userFilter0; // A filter instance used only to create new instances.

    /* Variables maintained to support logging of the proxy state and operations. */
    CryptLayer.CLLogID id;
    CryptLayer.CLLog logger;

    // ISO_8859_1 is specified as the Charset of stream readers and writers
    // so that they will treat each byte as one character.
    static final Charset charset8bit = StandardCharsets.ISO_8859_1;

    public CLSmtpProxy(CryptLayer.CLLogID id, InetAddress proxyHost, int proxyPort, InetAddress srvrHost, int srvrPort,
            int connMax, SmtpFilter userFilter0, CryptLayer.CLLog logger) {

        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.srvrHost = srvrHost;
        this.srvrPort = srvrPort;
        this.userFilter0 = userFilter0;

        this.connMax = connMax; // Max number of thread pairs active.
        this.connCount = new Semaphore(connMax);
        this.connWait = 0; // 1 if waiting for connection to port.

        this.logger = logger;
        this.id = new CryptLayer.CLLogID(id);
        this.id.type = CLLogID.SMTP;
        this.id.thread = CLLogID.CONTROL;
    }

    public void run() {
        ServerSocket listenSock = null;
        
        /* Create an unencrypted socket to receive client connection.
         * The client may have to be explicitly set up for"no encryption."
         */
        logger.log(id, CLLogID.INFO, "SMTP Listener Info");
        CryptLayer.infoLstnAddr(id, proxyHost, logger);
        try {
            listenSock = new ServerSocket(proxyPort, 1, proxyHost);
            do {
                Socket clntSock = null;
                SSLSocket srvrSock = null;
                
                // Listen for a new connection.
                instance ++;
                id.instance = instance;
                connCount.acquireUninterruptibly();
                connWait = 1;
                clntSock = listenSock.accept();         // Possible IOException.
                logger.log(id, CLLogID.ACCEPT, "Accepted SMTP client connection ");
                connWait = 0;
                
                // Connect an unencrypted socket to the SMTP server, then STARTTLS.
                
                Socket srvrSockRaw = new Socket(srvrHost, srvrPort);
                
                BufferedReader srvrRdrRaw =
                    new BufferedReader(new InputStreamReader(
                            srvrSockRaw.getInputStream(), charset8bit));
                BufferedWriter srvrWrtrRaw =
                    new BufferedWriter(new OutputStreamWriter(
                            srvrSockRaw.getOutputStream(), charset8bit));            
                            
                String fromSrvrRaw;
                fromSrvrRaw = srvrRdrRaw.readLine();
                
                logger.log(id, CLLogID.BODY, "SSC: " + fromSrvrRaw);
                
                srvrWrtrRaw.write("STARTTLS\r\n");
                srvrWrtrRaw.flush();
                fromSrvrRaw = srvrRdrRaw.readLine();            
                logger.log(id, CLLogID.BODY, "SSC: " + fromSrvrRaw);
                           
                // Connect a TLS socket to the SMTP server.
                SSLSocketFactory sslsocketfactory = 
                        (SSLSocketFactory) SSLSocketFactory.getDefault();
                srvrSock = (SSLSocket) sslsocketfactory.createSocket(
                        srvrSockRaw,
                        srvrHost.getHostAddress(), srvrPort, true );
                
                logger.log(id, CLLogID.INFO, "SMTP TLS Server Info");
                CryptLayer.infoSocketToSrvr(id, srvrSock, logger);
                
                logger.log(id, CLLogID.CONNECT, "Made SMTP server connection");
                
                /* Start subthreads for streams to and from the server. They terminate when
                 * either clntSock or srvrSock is closed.
                 */
                new CLSmtpConnection(id, clntSock, srvrSock, userFilter0, connCount, logger);
            } while(true); 
        } catch(IOException err) {
            logger.log(id, CLLogID.STOP, err);
            
        };
    }
}

/**
 * CLSmtpConnection manages one connection between an SMTP client and an SMTP
 * server. It accepts open sockets, one from an SMTP client and the other to an
 * SMTP server. It sets up two new threads, the SMTP client to server (SCS)
 * thread to pass the stream from the client socket to the server socket and a
 * companion SMTP server to client thread (SSC) thread to pass the stream from
 * the server socket to the client socket. It provides facilities for the stream
 * data to be filtered on the way through to implement cryptographic operations.
 * Also it allows the SSC thread to inject commands into the SCS stream and then
 * to intercept the server reply and to inject its own replies to client
 * commands. NOT IMPLEMENTED. If an exception occurs in either thread,
 * specifically when either the client or the server disconnects, that thread
 * closes its client and server sockets and terminates. Since the sockets are
 * closed, the companion thread will have an exception and terminate.
 */
final class CLSmtpConnection {

    static final byte[] BYTES_DATA = "DATA".getBytes();
    static final byte[] BYTES_END_DATA = ".\r\n\0".getBytes();
    static final byte[] BYTES_354 = "354 ".getBytes();
    static final byte[] BYTES_221 = "221".getBytes();
    static final byte[] BYTES_421 = "421".getBytes();
    
    /* This saves writing the fully qualified name over and over. */
    static final int BYTE_ARRAY_MAX = CLSmtpProxy.SmtpListOfByteArray.BYTE_ARRAY_MAX;

    /* Sent to client at start of session.*/
    private final byte[] BYTES_TO_CLNT_START = "220 dummy.teksavvy.com ESMTP\r\n".getBytes();

    private SmtpByteArrayReader clntRdr;
    private SmtpByteArrayWriter srvrWrtr;
    private SmtpByteArrayReader srvrRdr;
    private SmtpByteArrayWriter clntWrtr;
    private CLSmtpProxy.SmtpFilter userFilter;

    // Used by SmtpByteArrayReader()and SmtpByteArrayWriter() when sending a DATA command
    // to the server so that the SmtpFilter class does not have to wait for a 354 reply
    // from the server. See RFC9051-3.3.
    private Semaphore waitReply = new Semaphore(1);     // Released to end wait for 354 reply.
    private boolean is354 = false;                      // True if 354 reply received.

    private Semaphore connCount; // This is decremented (released) when connection closes.
    private CryptLayer.CLLogID id;
    private CryptLayer.CLLog logger;

    // ISO_8859_1 is specified as the Charset of stream readers and writers
    // so that they will treat each byte as one character.
    // static final Charset charset8bit = StandardCharsets.ISO_8859_1;

    /**
     * CLSmtpConnection manages one connection between an SMTP client and an
     * SMTP server. It accepts open sockets, one from an SMTP client and the
     * other to an SMTP server. It sets up two new threads, the SMTP client
     * to server (SCS) thread to pass the stream from the client socket to
     * the server socket and a companion SMTP server to client thread (ISC)
     * thread to pass the stream from the server socket to the client socket. 
     * It provides facilities for the stream data to be filtered on the way
     * through to implement cryptographic operations. Also it allows the SSC
     * thread to inject commands into the SCS stream and then to intercept
     * the server response and to inject its own responses to client commands.
     * (ABOVE NOT IMPLEMENTED). If an exception occurs in either thread,
     * specifically when either the client or the server disconnects, that
     * thread closes its client and server sockets and terminates. Since the
     * sockets are closed, the companion thread will have an exception and
     * terminate.

     */
    public CLSmtpConnection(CryptLayer.CLLogID id, Socket clntSock, Socket srvrSock,
            CLSmtpProxy.SmtpFilter userFilter0, Semaphore connCount, CryptLayer.CLLog logger) {
        
        this.connCount = connCount;
        this.id = new CryptLayer.CLLogID(id);
        this.logger = logger;
        
        /* Make a new instance of the filter. */
        userFilter = null;
        if (userFilter0 != null) userFilter = userFilter0.makeNewInstance();

        try {
            clntRdr = new SmtpByteArrayReader(clntSock, false);     // Possible IOException.
            srvrWrtr = new SmtpByteArrayWriter(srvrSock, false);    // Possible IOException.
            srvrRdr = new SmtpByteArrayReader(srvrSock, true);      // Possible IOException.
            clntWrtr = new SmtpByteArrayWriter(clntSock, true);     // Possible IOException.
        } catch (IOException err) {
            logger.log(this.id, CLLogID.EXCP, err);
        }
        
        Thread toSrvr = new Thread( new SmtpClntToSrvr());
        
        Thread toClnt = new Thread( new SmtpSrvrToClnt());
        
        toSrvr.start();
        toClnt.start();
        
        logger.log(id, CLLogID.CONN, "SMTP connection started");
    }
    
    private class SmtpClntToSrvr implements Runnable {
        CryptLayer.CLLogID idCS;
       
        public SmtpClntToSrvr() {
            idCS = new CryptLayer.CLLogID(id);
            idCS.thread = CLLogID.TOSRVR;
            return;
        }
        
        public void run() {
            CLSmtpProxy.SmtpListOfByteArray command = new CLSmtpProxy.SmtpListOfByteArray();
            CLSmtpProxy.SmtpListOfByteArray replyToInject = new CLSmtpProxy.SmtpListOfByteArray();

            int logEvent = 0;
            
            logger.log(idCS, CLLogID.START, "SCS running");
        
            try {
                // Receive commands from client and pass to server.
                while (true) {
                    clntRdr.getCommandOrReply(command);
                    
                    if (command.size() == 0) break;
                    logger.log(idCS, CLLogID.BODY, command);
                     
                    if (userFilter != null) {           // Implement filtering.
                        replyToInject.clear();
                        
                        userFilter.toSmtpServer(command, replyToInject);
                        
                        if (replyToInject.size() > 0) clntWrtr.sendCommandOrReply(replyToInject);
                    }
                    
                    srvrWrtr.sendCommandOrReply(command);   // Possible IOException.
                }
            } catch (IOException err) {
                logger.log(idCS, logEvent, err);
            }
            
            // Null or zero length or exception indicates client or server
            // has disconnected. Terminate thread after closing both sockets
            // to trigger termination of the server to client thread.
            try {
                clntRdr.close();
            } catch(IOException err) { 
                logger.log(idCS, CLLogID.EXRDCLS, err);
            }
            try {
                srvrWrtr.close();
            } catch(IOException err) { 
                logger.log(idCS, CLLogID.EXWRCLS, err);
            }
            logger.log(idCS, CLLogID.STOP, "SCS stopped");

        }
    }

    private class SmtpSrvrToClnt implements Runnable {
        CryptLayer.CLLogID idSC;
        
        public SmtpSrvrToClnt() {
            idSC = new CryptLayer.CLLogID(id);
            idSC.thread = CLLogID.TOCLNT;
            return;
        }
        
        public void run() {
            
            CLSmtpProxy.SmtpListOfByteArray reply =  new CLSmtpProxy.SmtpListOfByteArray();
            CLSmtpProxy.SmtpListOfByteArray commandToInject = new CLSmtpProxy.SmtpListOfByteArray();

            int logEvent = 0;
            String logExcept = "";
            
            logger.log(idSC, CLLogID.START, "SSC running");
            try {
                clntWrtr.write(BYTES_TO_CLNT_START);
                logger.log(idSC, CLLogID.BODY, new String(BYTES_TO_CLNT_START));
                
                // Receive replies from server and pass to client.
                while(true) {
                    srvrRdr.getCommandOrReply(reply);
                    // if (reply.size() == 0) break;
                    logger.log(idSC, CLLogID.BODY, reply);
                    
                    if (userFilter != null) {           // Implement filtering.
                        commandToInject.clear();
                        
                        userFilter.toSmtpServer(reply, commandToInject);
                        
                        if (commandToInject.size() > 0) srvrWrtr.sendCommandOrReply(commandToInject);
                    }
                    
                    clntWrtr.sendCommandOrReply(reply);

                    if(reply.startsWith(BYTES_221) || reply.startsWith(BYTES_421)) break;
                }                    
            } catch(Exception err) {
                logExcept = "Except: " + err;
            }
            logger.log(idSC, logEvent, "SSC stop " + logExcept);
            
            // Null or zero length or exception indicates client or server
            // has disconnected. Terminate thread after closing both sockets
            // to trigger termination of the client to server thread.
            try {
                clntWrtr.close();
            } catch(Exception err) {
                logger.log(idSC, CLLogID.EXRDCLS, "SSC except: " + err);
            }
            try {
                srvrRdr.close();
            } catch(Exception err) {
                logger.log(idSC, CLLogID.EXWRCLS, "SSC except: " + err);
            }
            
            // Make sure that the client to server thread is terminated.
            //try {
            //    flags.clntToSrvrThrd.join();
            //} catch(Exception err) {
            //    logger.log(idSC, CLLogID.EXJOIN, "SSC join except: " + err);
            //}
            connCount.release();
            logger.log(idSC, CLLogID.STOP, "SSC and SCS stop");
        }
        
    }
    
    /**
     * CLSMtpConnection.SmtpByteArrayReader receives a byte stream from
     * an SMTP client or server.
     */
    private class SmtpByteArrayReader extends BufferedInputStream {
        
        boolean isFromServer;
        
        public SmtpByteArrayReader(Socket socket, boolean isFromServer) 
                throws IOException {
            super(new BufferedInputStream(socket.getInputStream(), 2 * BYTE_ARRAY_MAX));
            this.isFromServer = isFromServer;
        }
        
        /**
         * CLSmtpConnection.SmtpByteArrayReader.getCommandOrReply receives
         * a byte stream from an SMTP client or server and stores it as a
         * list of byte arrays. Lines longer than BYTE_ARRAY_MAX are stored
         * in multiple byte arrays with all but the last having length
         * BYTE_ARRAY_MAX, and the last having length < BYTE_ARRAY_MAX
         * (possibly 0). One call reads a complete command or reply, which
         * may consist of multiple lines. When a DATA command is received from
         * the client, a 354 reply is injected in the server to client stream
         * to make the client send the rest of the command. The end of the DATA
         * command is marked by "\r\n.\r\n". All other commands consist of a
         * single line. The last line of a multiline reply is indicated by ' '
         * in the fourth character of the line. See RFC5321-4.2.1.
         */
        public void getCommandOrReply(CLSmtpProxy.SmtpListOfByteArray commandOrReply)
                 throws IOException {
            
            byte[] oneArray;
            byte[] lineStart;
            int arrayLength;
            byte oneByte = 0;
            boolean moreData = false;
            boolean moreReply = false;
            
            do {
                lineStart = null;
                do {                                    // Read one line (which may be longer than BYTE_ARRAY_MAX).
                    this.mark(BYTE_ARRAY_MAX);
                    arrayLength = 0;
                    if (isFromServer) System.out.print("Waiting for server read\r\n"); // *** DEBUG
                    do {                                // Search for EOL, stopping at BYTE_ARRAY_MAX bytes.
                        oneByte = (byte) this.read();
                        if (oneByte <= 0) throw new IOException();
                        arrayLength++;
                    } while (arrayLength < BYTE_ARRAY_MAX && oneByte != '\n');
                    this.reset();                       // Go back to start of line. Possible IOException.
                    
                    oneArray = new byte[arrayLength];
                    if (this.read(oneArray, 0, arrayLength) < arrayLength) throw new IOException();
                    System.out.print("Read  " + isFromServer + " " + oneArray.length + " >" + new String(oneArray) + "<\r\n"); // *** DEBUG
                    commandOrReply.add(oneArray);   // Read to EOL or BYTE_ARRAY_MAX
                    if (lineStart == null) lineStart = Arrays.copyOf(oneArray, 4);  // Command name or reply code.
                } while (oneByte != '\n');
                // If line length was exact multiple of BYTE_ARRAY_MAX, mark end of line with 0 length array.
                if (arrayLength == BYTE_ARRAY_MAX) commandOrReply.add(new byte[0]);
                
                if (!isFromServer) {
                    // If the first line of a DATA command from client, inject 354 reply to client here so client
                    // will send the rest of the command.
                    if (!moreData && commandOrReply.startsWith(BYTES_DATA)) {
                        moreData = true;                // DATA is the only multiline command.
                        clntWrtr.write("354 Injected\r\n".getBytes());
                    } else if (moreData && Arrays.equals(lineStart, BYTES_END_DATA)) {
                        moreData = false;
                    }
                } else {
                    // If the first line of a reply from the server, end any C to S wait for reply to DATA command
                    // and check if this is a 354 reply.
                    if ( !moreReply ) {
                        is354 = commandOrReply.startsWith(BYTES_354);
                        if (waitReply.availablePermits() == 0) waitReply.release();
                        // Any 354 reply from the server is discarded.
                        if (is354) commandOrReply.clear();
                    }
                    moreReply = lineStart[3] != ' ';
                }
            } while (moreData || moreReply);
        }
    }
    /**
     * CLSMtpConnection.SmtpByteArrayWriter sends a byte stream to
     * an SMTP server or client.
     */
    private class SmtpByteArrayWriter extends BufferedOutputStream {
        
        boolean isToClient;
        
        public SmtpByteArrayWriter(Socket socket, boolean isToClient) 
                throws IOException {
            super(new BufferedOutputStream(socket.getOutputStream(), 2 * BYTE_ARRAY_MAX));
            this.isToClient = isToClient;
        }
        
        /**
         * CLSmtpConnection.SmtpByteArrayWriter.write() provides a synchronized
         * version of the BufferedOutputStream.write() and flushes after writing.
         */
        public void write(byte[] oneArray)
                 throws IOException {
            
            synchronized(this) {
                write(oneArray, 0, oneArray.length);
                flush();
                System.out.print("Write " + isToClient + " " + oneArray.length + " >" + new String(oneArray) + "<\r\n"); // *** DEBUG
            }
        }
        
        /**
         * CLSmtpConnection.SmtpByteArrayWriter.sendCommandOrReply() takes
         * list of byte arrays which contain a complete command or reply
         * and sends it to the server or client. In the case of a DATA command,
         * it waits for a server reply after sending the first line. If the
         * reply is not 354 the remainder of the command is discarded.
         */
        public void sendCommandOrReply(CLSmtpProxy.SmtpListOfByteArray commandOrReply)
                 throws IOException {
            
            boolean isDATA = ! isToClient && commandOrReply.startsWith(BYTES_DATA);
            
            synchronized(this) {
                if (isDATA) {                           // First line of DATA command.
                    waitReply.acquireUninterruptibly();
                    write(commandOrReply.remove());     // Possible IOException.
                    flush();
                    waitReply.acquireUninterruptibly(); // Wait for the server reply.
                    // Don't send the DATA body if the reply is not 354.
                    if (!is354) commandOrReply.clear();
                    if (isDATA) waitReply.release();
                }
                while (commandOrReply.size() > 0) {     // Send command to server.
                    write(commandOrReply.remove());     // Possible IOException.
                }
                flush();
            }
        }
    }
}
