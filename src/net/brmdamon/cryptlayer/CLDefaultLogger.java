/* Copyright (C) 2021 Jack Dodds
 * This code is part of CryptLayer and is hereby released under the
 * Gnu General Public Licence v.3.
 * See GPLv3.txt and https://www.gnu.org/licenses/gpl-3.0.txt
*/
package net.brmdamon.cryptlayer;

import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import net.brmdamon.cryptlayer.CryptLayer.CLLogID;

/* All the imported packages must be available on all target platforms:
 * specifically Debian, Android, Windows and (we hope) iOS.
 */






/** CLDefaultLogger provides default methods for sending messsages to a
 * log file. For simple command line execution, it be used as is.
 * It can be extended to modify the logger behaviour or to capture
 * information for use in the user interface, for example.
 */

public class CLDefaultLogger implements CryptLayer.CLLog {

    private int imapInstanceCount = 0;
    private int smtpInstanceCount = 0;
    private PrintStream logStream = System.out;
    
    public boolean logOpen() {
        return true;
    }
    
    public boolean logOpen(PrintStream logStream) {
        this.logStream = logStream;
        return true;
    }
    
    /** This updates the counts of IMAP and SMTP connection instances and returns
     * the appropriate one for printing in the log.
     */
    public int updateInstanceCount(CryptLayer.CLLogID id, int event) {
        
        int connCount = 0;
        
        if (event == CLLogID.CONN && id.thread == CLLogID.CONTROL) {
            if (id.type == CLLogID.IMAP) imapInstanceCount++;
            if (id.type == CLLogID.SMTP) smtpInstanceCount++;
        }
        if (event == CLLogID.STOP && id.thread == CLLogID.TOCLNT) {
            if (id.type == CLLogID.IMAP) imapInstanceCount--;
            if (id.type == CLLogID.SMTP) smtpInstanceCount--;
        }
        if (id.type == CLLogID.IMAP) connCount = imapInstanceCount;
        if (id.type == CLLogID.SMTP) connCount = smtpInstanceCount;
        
        return connCount;
    }
    
    
    public synchronized boolean log(CryptLayer.CLLogID id, int event, Exception err) {
        
        StackTraceElement[] trace = err.getStackTrace();
        int depth = trace.length - 1;
        
        if (depth >= 1 && trace[depth].getClassName().contains("java.lang.Thread")) depth--;
        // StackTraceElement traceEnd = trace[ trace.length - 1];
        //for (int i = 0; i < trace.length; i++) {
        log(id, event, "Exception: " + trace[depth].getClassName() + "."
                + trace[depth].getMethodName() + ":" + trace[depth].getLineNumber());
        //}
        return true;
    }
    
    public synchronized boolean log(CryptLayer.CLLogID id, int event, CLSmtpProxy.SmtpListOfByteArray message) {
        
        int size = message.size();
        String messageLog = "";
        
        if (size >= 1) messageLog += new String(message.getFirst());    // Log first line of command or response.
        if (id.logLevel == 0) {
            for (int index = 1; index < size - 1; index ++) {           // Log entire command or response.
                messageLog += new String(message.get(index));
            }
        }
        if (size >= 3) messageLog += " ... ";
        if (size >= 2) messageLog += new String(message.get(size - 1)); // Log last line of command or response.
        
        log(id, event, messageLog);
        
        return true;
    }
    
    public synchronized boolean log(CryptLayer.CLLogID id, int event, LinkedList<CLImapProxy.ImapByteArray> message) {
        
        int size = message.size();
        String messageLog = "";
        
        if (size >= 1) messageLog += new String(message.getFirst().body);    // Log first line of command or response.
        if (id.logLevel == 0) {
            for (int index = 1; index < size - 1; index ++) {           // Log entire command or response.
                messageLog += new String(message.get(index).body);
            }
        }
        if (size >= 3) messageLog += " ... ";
        if (size >= 2) messageLog += new String(message.get(size - 1).body); // Log last line of command or response.
        
        log(id, event, messageLog);
        
        return true;
    }
    
    public synchronized boolean log(CryptLayer.CLLogID id, int event, String messageLog) {
        
        final SimpleDateFormat logTimeFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        int connCount;
        
        if (id == null || event < id.logLevel) return true;
        
        connCount = updateInstanceCount(id, event); 
        
        if (id.logLevel > 0) {
            int index1 = messageLog.indexOf("\r\n");
            int index2 = messageLog.lastIndexOf("\r\n", messageLog.length() - 3);
            
            if (index1 < index2)
                messageLog = messageLog.substring(0, index1) + "\r\n    ... " + messageLog.substring(index2).trim();
        }
        logStream.print(logTimeFmt.format(new Date()) + " " + id.name + " P" + id.type + " I" + id.instance +
                    " T" + id.thread + " E" + event + " C" + connCount + " " + messageLog);
        if (!messageLog.endsWith("\r\n")) System.out.println();
        return true;
    }
    
    public boolean logClose() {
        return true;
    }
}
