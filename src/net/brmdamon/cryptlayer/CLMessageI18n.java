/* Copyright (C) 2021 Jack Dodds
 * This code is part of CryptLayer and is hereby released under the
 * Gnu General Public Licence v.3.
 * See GPLv3.txt and https://www.gnu.org/licenses/gpl-3.0.txt
*/
package net.brmdamon.cryptlayer;

/** CLMessageI18n provides a hook for internationalizing log messages, GUI
 * text, etc. The message written by the developer is used by the static
 * method __() (two underscores) as a key to look up the equivalent message
 * in the language of the current locale, if one is specified, otherwise the
 * original message is returned. Based on the similar construct in Python.
 * For maximum convenience, use static import:
 * import static net.brmdamon.cryptlayer.CLMessageI18n.__;
 */
public class CLMessageI18n {
    /* This version is a stub that returns the original message. */

    static public String __(String text) {
        
        return text;
    }

    static public String __(Exception except) {
        
        return except.toString();
    }

}
