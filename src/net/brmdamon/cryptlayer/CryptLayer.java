/* Copyright (C) 2021 Jack Dodds
 * This code is part of CryptLayer and is hereby released under the
 * Gnu General Public Licence v.3.
 * See GPLv3.txt and https://www.gnu.org/licenses/gpl-3.0.txt
*/
package net.brmdamon.cryptlayer;

/* All the imported packages must be available on all target platforms:
 * specifically Debian, Android, Windows and (we hope) iOS.
 */
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Properties;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;

// import java.nio.file.Path;
// import java.nio.file.Paths;

/** CryptLayer is an IMAP and SMTP Submit proxy which sits between an email client and the
 * IMAP and SMTP submit servers provided by the ISP in order to implement encryption.
 * A CryptLayer instance can use at most one IMAP server user ID.
 * It can maintain multiple simultaneous IMAP and SMTP connections.
 * Each connection is serviced by two threads: client to server and server to client.
 * Parameter names in CryptLayer may use abbreviations of certain common words for brevity:
 * client: clnt, server: srvr, connection: conn, listener: lstn, domain: domn, configuration: conf,
 * socket: sock, reader:rdr, writer:wrtr, thread: thrd, command: cmnd, response: resp.
 */
public class CryptLayer {
    
    private CLImapProxy imapProxy = null;
    private CLSmtpProxy smtpProxy = null;
    private Thread imapThread;
    private Thread smtpThread;

    // All data used by CryptLayer including keys is stored in appDir.
    // Path appDir = Paths.get(System.getProperty("user.home"),"CryptLayer");
    private static File appDir = new File(new File(System.getProperty("user.home")), "CryptLayer");
    
    Properties configuration;                       // Configuration parameters that define the proxy:
    private String name = "";
    private Integer logLevel = 0;
    private String imapLstnDomn = "";
    private Integer imapLstnPort = 0;
    private String imapSrvrDomn = "";
    private Integer imapSrvrPort = 0;
    private String smtpLstnDomn = "";
    private Integer smtpLstnPort = 0;
    private String smtpSrvrDomn= "";
    private Integer smtpSrvrPort = 0;
    
    private CLLog logger;                           // Logger for events and status.
    private CLLogID id;                             // Identifier to logger for the root thread.
    
    /** A Cryptlayer instance is configured using parameters in a file identified by name
     * relative to the user's home directory. A default logger is built into CryptLayer.
     */
    public CryptLayer(Properties configuration) {
        this.configuration = configuration;
        this.logger = (CLLog) new CLDefaultLogger();
    }

    /** The default logger can be overridden and the logger information can be used to 
     * drive an interactive interface.
     */
    public CryptLayer(Properties configuration, CLLog logger) {
        this.configuration = configuration;
        this.logger = logger;
    }
    
    /** Loads the configuration from file confFileName and starts the IMAP and
     * SMTP submit proxies .
     */
    public void launchProxies() {
        
        InetAddress imapLstnAddr, imapSrvrAddr, smtpLstnAddr, smtpSrvrAddr;
        
        // Create the identifier for logging purposes.
        id = new CryptLayer.CLLogID("[unconfigured]");
        
        // Load the configuration.
        loadConf();
        
        id.name = name;
        id.logLevel = logLevel;
        
        try {
            // Start the IMAP proxy.
            imapSrvrAddr = InetAddress.getByName(imapSrvrDomn);
            imapLstnAddr = InetAddress.getByName(imapLstnDomn);
            CryptLayer.infoLstnAddr(id, imapLstnAddr, logger);
            imapProxy = new CLImapProxy(id, imapLstnAddr, imapLstnPort, imapSrvrAddr, imapSrvrPort, 10, null, logger );
            imapThread = new Thread(imapProxy);
            imapThread.start();
            
            // Start the SMTP Submit proxy.
            smtpSrvrAddr = InetAddress.getByName(smtpSrvrDomn);
            smtpLstnAddr = InetAddress.getByName(smtpLstnDomn);
            CryptLayer.infoLstnAddr(id, smtpLstnAddr, logger);
            
            smtpProxy = new CLSmtpProxy(id, smtpLstnAddr, smtpLstnPort, smtpSrvrAddr, smtpSrvrPort, 10, null, logger);
            smtpThread = new Thread(smtpProxy);
            smtpThread.start();
        } catch (Exception err) {
            logger.log(id, CLLogID.INFO, err.toString());
        }
    }
    
    public void stop() {
        
    }
    
    public void waitDone() {
        
        try {
            // Wait for IMAP and SMTP proxies to terminate.
            
            imapThread.join();
            smtpThread.join();
            
        } catch (Exception err) {
            logger.log(id, CLLogID.INFO, err.toString());
        }
        
        try {
            logger.logClose();
        } catch (Exception err) {};
        
    }
    
    /** Get and validate the configuration parameters.
     */
    void loadConf() {
        
        name = configuration.getProperty("name", "default");
        logLevel =     Integer.valueOf(configuration.getProperty("logLevel", "0"));
        imapLstnDomn = configuration.getProperty("imapLstnDomn", "localhost");
        imapLstnPort = Integer.valueOf(configuration.getProperty("imapLstnPort","1143"));
        imapSrvrDomn = configuration.getProperty("imapSrvrDomn", "localhost");
        imapSrvrPort = Integer.valueOf(configuration.getProperty("imapSrvrPort", "0"));
        smtpLstnDomn = configuration.getProperty("smtpLstnDomn", "localhost");
        smtpLstnPort = Integer.valueOf(configuration.getProperty("smtpLstnPort","1587"));
        smtpSrvrDomn = configuration.getProperty("smtpSrvrDomn", "");
        smtpSrvrPort = Integer.valueOf(configuration.getProperty("smtpSrvrPort", "0;"));
        
        logger.log(id, CLLogID.INFO, "Configuration:\n" + logLevel + "\n" +
                imapLstnDomn + ":" + imapLstnPort + "\n" + imapSrvrDomn + ":" + imapSrvrPort + "\n" +
                smtpLstnDomn + ":" + smtpLstnPort + "\n" + smtpSrvrDomn + ":" + smtpSrvrPort);
                
        
    }
    
    /** Get information about a server.
     */
    public static void infoSocketToSrvr(CLLogID id, SSLSocket socketToSrvr, CLLog logger) {
        
        SSLSession srvrSession = socketToSrvr.getSession();
        
        String srvrHost = srvrSession.getPeerHost();
        int srvrPort = srvrSession.getPeerPort();
        String srvrCipher = srvrSession.getCipherSuite();
        String srvrProtocol = srvrSession.getProtocol();
        String srvrIDname;
        
        try {
            srvrIDname = srvrSession.getPeerPrincipal().toString();
        } catch (Exception error) {
            srvrIDname = "Error: " + error;
        }
        
        logger.log(id, CLLogID.INFO, "Opened port: " + srvrHost + ":" + srvrPort);
        logger.log(id, CLLogID.INFO, "Host: " + srvrIDname);
        logger.log(id, CLLogID.INFO, "Cipher: " + srvrCipher);
        logger.log(id, CLLogID.INFO, "Protocol: " + srvrProtocol);
        return;
    }
    
    /** Get information about a CryptLayer listening socket.
     */
    public static void infoLstnAddr(CLLogID id, InetAddress lstnAddr, CLLog logger)
    {
        String hostName = lstnAddr.getHostName();
        String canonicalName = lstnAddr.getCanonicalHostName();
        String hostAddress = lstnAddr.getHostAddress();          
        
        logger.log(id, CLLogID.INFO, "Lstningname: " + hostName);
        logger.log(id, CLLogID.INFO, "Canonicalname: " + canonicalName);
        logger.log(id, CLLogID.INFO, "Address: " + hostAddress);
        return;

    }
    
    /** For command line execution.
     * Optional argument: Configuration file name.
     */
    
    public static void main(String[] args) {
        
        Properties configuration = new Properties();
        File applicationHome;
        String configurationFileName = "default.conf";
        
        System.out.println("This is CryptLayer. Copyright (C) 2021 Jack Dodds");
        System.out.println("This program comes with ABSOLUTELY NO WARRANTY.");
        System.out.println("This is free software, and you are welcome to ");
        System.out.println("redistribute it under certain conditions.");
        System.out.println("See https://www.gnu.org/licenses/gpl-3.0.txt");
        System.out.println("");
        
        if (args.length >= 1) configurationFileName = args[0];
        
        // A platform dependent application home should be defined here.
        // This one is good for Linux.
        applicationHome = new File(System.getProperty("user.home"), ".cryptlayer");
        
        try {
            if(configurationFileName.charAt(0) != '/') {
                configuration.load(new FileReader(new File(applicationHome, configurationFileName)));
            } else {
                // Provide for configuration file in a non-standard location.
                configuration.load(new FileReader(configurationFileName));
            }
        } catch (Exception err) {
            System.out.print("Error loading configuration: " + err);
            return;
        }

        CryptLayer proxy = new CryptLayer(configuration);

        proxy.launchProxies();
        proxy.waitDone();
        
    }
    
    /** The CLLog interface lets CryptLayer send information about its status
    * to a file or user interface. Objects of CLLogID are used to identify the
    * thread that is sending the status. 
    * CryptLayer implements a default CLLog, but an external implementation
    * can be passed to the CryptLayer constructor if needed.
    */
    public interface CLLog {
        boolean logOpen();
        boolean logOpen(PrintStream logStream);
        boolean log(CLLogID id, int event, String comment);
        boolean log(CLLogID id, int event, Exception err);
        boolean log(CLLogID id, int event, CLSmtpProxy.SmtpListOfByteArray message);
        boolean log(CLLogID id, int event, LinkedList<CLImapProxy.ImapByteArray> message);
        boolean logClose();
    }

    /** CLLog uses a standard identifier to indicate the thread and the event producing the message. */
    public static class CLLogID {
        
        public static final int MAIN = 0;                    // Proxy types.
        public static final int IMAP = 1;
        public static final int SMTP = 2;
        
        public static final int ROOT = 0;                    // Proxy thread types.
        public static final int CONTROL = 1;
        public static final int TOSRVR = 2;
        public static final int TOCLNT = 3;
                                                        // CLLog.log() event parameter values.
        public static final int EXCP = 60;                   // Unexpected exception.
        public static final int INFO = 55;                   // Information message.
        public static final int CONN =  54;                  // Starting connection.
        public static final int STOP =   53;                 // Stopping stream thread.
        public static final int START =  52;                 // Starting stream thread.
        public static final int RUN =  51;                   // Running stream thread.
        public static final int EXOPENLSTN = 41; // Exception opening listen socket.
        public static final int EXOPENSRVR = 40; // Exception opening server socket.
        public static final int EXOPENCLNT = 39; // Exception opening client socket.
        public static final int EXJOIN = 38; // Exception joining thread.
        public static final int EXRDNEW = 37;// Exception getting reader.
        public static final int EXWRNEW = 36;// Exception getting writer.
        public static final int EXRDCLS = 35;// Exception closing reader.
        public static final int EXWRCLS = 34;// Exception closing writer.
        public static final int EXRD = 33;   // Exception reading.
        public static final int EXWR = 32;   // Exception writing.
        public static final int ACCEPT = 27; // Accepted client connection.
        public static final int CONNECT = 26;// Made server connection.
        public static final int RDNULL = 25; // Null string reading.
        //public static final int RDEMPTY = 24;// Empty string reading.
        //public static final int RPLAIN = 13; // Received plaintext email.
        //public static final int RCRYPT = 14; // Received/decrypted email.
        //public static final int SPLAIN = 15; // Sent plaintext email.
        //public static final int SCRYPT = 16; // Sent/encrypted email.
        public static final int BODY = 18;   // Body of command or response.
        //public static final int SCMND = 11;  // Sent command (not one of above)..
        //public static final int RSPNS = 12;  // Received response (not above).
        
        public String name = "";        // Configuration name.
        public int type = 0;            // Proxy type.
        public int instance = 0;        // Instance counter for the proxy type.
        public int thread = 0;          // Proxy thread type.
        public int logLevel = 0;        // Minimum event parameter logged.
        
        public CLLogID( String name ) {
            this.name = name;
            this.type = MAIN;
            this.instance = 0;
            this.thread = ROOT;
            this.logLevel = 0;
        }
        
        public CLLogID( CLLogID id ) { 
            this.name = id.name;
            this.type = id.type;
            this.instance = id.instance;
            this.thread = id.thread;
            this.logLevel = id.logLevel;
        }
    }
}
