#CryptLayer

CryptLayer sits between an email client and an ISP's IMAP and SMTP Submit servers and filters the command and response messages that pass between them. The goal is to enable end-to-end encryption for clients that do not support encryption.

##### Coding style
Based (without being rigid) on the Google Java Style Guide but with some changes which are intended to reduce the number of lines used for an algorithm so more of it can be displayed on a vertical screen.

* A source file contains exactly one public class but (as in Oracle style) when non-public classes and interfaces are associated with a public class, they can be in the same source file as the public class. The public class should be the first class or interface in the file.

* Maximum line length is increased to 120 (from 80), except in comments that become part of JavaDoc files, which should be 80 max. (Current displays are wide enough for two 120 character wide windows for side-by-side comparison.)

* Nested classes/interfaces placed at the end of the parent class (or wherever appropriate).

* Single line control statements may be used. Example:  _if (index < 0) index = 0;_

* The use of private attributes with accessor methods is optional. Balance improved clarity and security against the increased amount of code.

* A parameter of a class constructor method may "hide" a member of the class that has the same name. Typically the parameter is used to initialize the member. (Even though  _checkstyle_  complains when  _any_  parameter hides a member.)

* Declaring a parameter "final" is optional even if it is not modified. Saves clutter.

* Names of entities may use abbreviations of certain common words for brevity: client: clnt, server: srvr, connection: conn, listener: lstn, domain: domn, configuration: conf, socket: sock, reader:rdr, writer:wrtr, thread: thrd.

* Comments are designed to produce a useful Javadoc.

Source code can be style checked using  _checkstyle_  but results need not be rigidly obeyed. Example:

_checkstyle -c brmdamon_checks.xml src/net/brmdamon/cryptlayer/CLImapProxy.java |less_
