package net.brmdamon.clandroid;

import android.app.Application;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import java.lang.Thread;
import java.lang.StringBuilder;
import java.util.Date;
import java.text.SimpleDateFormat;
import android.os.Looper;
import android.os.Handler;
import android.os.Message;



import net.brmdamon.cryptlayer.*;



import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.security.Principal;
import java.net.InetAddress;
import java.io.FileWriter;
import java.io.File;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Properties;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Collection;





public class CLAndroid extends Application {
    
    static String confFilePath = "/storage/emulated/0/Download/cryptlayer.conf";
    static String logFilePath = "/storage/emulated/0/Logs/cryptlayer.log";
    static BackgroundCL backgroundCL;	// Background thread - runs CryptLayer.
    static String appData;				// Messages from background to UI.
    static Handler mainCLHandler = null;// Log data from background to UI.
    static List<String> logHistory = new LinkedList<String>();
    
    
    @Override
    public void onCreate() {
        super.onCreate();
        
        // Create and start the background thread.
        backgroundCL = new BackgroundCL();
        backgroundCL.start();
    }


    public static class MainCLActivity extends Activity 
                                        implements Handler.Callback{
        
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            
            mainCLHandler = new Handler(Looper.myLooper(), this);
            
            //try {
            //Thread.sleep(100); // *** DEBUG Major kludge
            //} catch(Exception e) {
            //	
            //}
            
            setContentView(R.layout.activity_main);
            
            TextView bgMsgView = (TextView) findViewById(R.id.bg_msg_view);
            bgMsgView.setText("onCreate:\n"+ appData);
        }
        
        @Override
        public void onDestroy() {
            super.onDestroy();
            mainCLHandler = null;
        }
        
        @Override
        public void onRestart() {
            super.onRestart();

            TextView bgMsgView = (TextView) findViewById(R.id.bg_msg_view);
            bgMsgView.setText("onRestart:\n" + appData);
        }

        @Override
        public boolean handleMessage(Message msg) {
            
        	// Truncates message to two lines maximum, 160 char maximum.
        	// appends it to the logHistory, and deletes lines exceeding
        	// maxHistoryLines from the end of logHistory, then displays
        	// logHistory.
        	
        	try {
        	
        	String historyMsg = (String) msg.obj;
        	String[] msgSplit = historyMsg.trim().split("\n", 3);
        	String msgTrim;
        	
        	for(int line = 0; line < 2 && line < msgSplit.length - 1; line ++) {
        		msgTrim = msgSplit[line].trim();
        		if(msgTrim.length()>0) {
        			logHistory.add(msgTrim);
        			if(logHistory.size() > 10)
        				logHistory.remove(0);
        		}
        	}
        	
        	
        	
        	StringBuilder stringb = new StringBuilder(1000);
        	for(int line = 0; line < logHistory.size(); line ++)
        		stringb.append(logHistory.get(line) + "\n");
        	
        	
            TextView logView = (TextView) findViewById(R.id.log_view);
            logView.setText(stringb.toString());
            
        	} catch(Exception err) {
                TextView logView = (TextView) findViewById(R.id.log_view);
                logView.setText("Exception: " + err);
        	}
            
            return true;
        }    
    }
    
    
    
    static Integer logLevel = 0;
    
    
    public class BackgroundCL extends Thread {
    	
        //FileWriter logFile;
        
    	BackgroundCL() {
            appData = "Background - creating:\n";
        }
        
        @Override
	    public void run() {
        	
            Properties configuration = new Properties();
        	FileReader confFile;
	    	
        	CLAndroidLogger logger = new CLAndroidLogger();
	
	        CryptLayer clProxy;
        	
	        try {
	            
	            // Open the log file.
	            appData = "CryptLayer - opening log file:\n";
	            logger.logOpen();
	            appData = "CryptLayer - opened log file:\n";
	            
	            // Read the configuration file.
                configuration.load(new FileReader(confFilePath));
	            
	            // Start the proxy.
	        	clProxy = new CryptLayer(configuration, logger);
	        	clProxy.launchProxies();
	        	
	        	// Wait for the proxy to complete.
	        	clProxy.waitDone();
	        	appData = "CryptLayer - done:\n";
                
	            logger.logClose();
	        } catch(Exception error) {
	        	appData = "CryptLayer log exception" + error;
	        };
	        
	    }
    }
    
    
    public class CLAndroidLogger extends CLDefaultLogger {
        
        PrintStream logStream;
        private File appDir = new File(getApplicationInfo().dataDir);

        @Override
        public boolean logOpen() {
            try {
                // *** FIXME Need to check that the folder exists
                logStream = new PrintStream(logFilePath);
            } catch(Exception error) {
                appData = "Error opening log file: " + error;
                return false;
            }
            
            //***
            logStream.print("AAAAA\n");
            logStream.print(appDir);
            logStream.print("BBBBB\n");
            logStream.print(appDir.isFile());
            logStream.print("CCCCC\n");
            logStream.print(appDir.setReadable(true, false));
            logStream.print("DDDDD\n");
            
            
            return true;
        };
        
        public boolean logOpen(String logFileName) {
            try {  
                logStream = new PrintStream(new File(appDir, logFileName));
            } catch (Exception err) {
                System.out.println(err);
            }
            return true;
        }
        
        @Override
        public synchronized boolean log(CryptLayer.CLLogID id, int event, String messageLog) {
            
            final SimpleDateFormat logTimeFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            int connCount = 0;
            
            if (id == null || event < id.logLevel) return true;
            
            connCount = updateInstanceCount(id, event); 
            
            // Delete all but first and last line of message unless logLevel == 0.
            if (id.logLevel > 0) {
                int index1 = messageLog.indexOf("\r\n");
                int index2 = messageLog.lastIndexOf("\r\n", messageLog.length() - 3);
                
                if (index1 < index2)
                    messageLog = messageLog.substring(0, index1) + "\r\n    ... " + messageLog.substring(index2).trim();
            }

            
            String logLine;
            logLine = logTimeFmt.format(new Date()) + " " + id.name + " P" + id.type + " I" + id.instance +
                    " T" + id.thread + " E" + event + " C" + connCount + " " + messageLog + "\n";
            
            // Write time stamp, level, config name and message to log file.
            try {
                if(id.logLevel >= logLevel) {       
                    logStream.print(logLine);
                    logStream.flush();
                    
                    appData = logLine+ "\n";
                }
            } catch(Exception err) {
                appData = "Background log file exception" + err;
                return false;
            }
            
            // Send a possibly truncated version to the UI.
            try {
                // It appears that this point can be reached before the
                // handler is initialized. If so just skip.
                Message handlerMsg = new Message();
                if(logLine.length() > 80)
                    logLine = logLine.substring(0, 80) + " ...";
                handlerMsg.what = 0;
                handlerMsg.arg1 = id.logLevel;
                handlerMsg.arg2 = 0;
                handlerMsg.obj = logLine;
                mainCLHandler.sendMessage(handlerMsg);
            } catch(Exception err) {
                appData = "Handler exception:" + err;
            }
            
            return true;
        }
        
        @Override
        public boolean logClose() {
            try {
                logStream.close();
            } catch(Exception error) {
                appData = "Error closing log file: " + error;
                return false;
            }
            return true;
        }

        
        
    }
    
    
    
    
    
}
